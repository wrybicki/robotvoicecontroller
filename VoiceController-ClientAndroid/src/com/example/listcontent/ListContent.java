package com.example.listcontent;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.util.Log;

/**
 * Helper class for providing sample content for user interfaces 
 */

public class ListContent 
{

	public static List<PandaRobot> ITEMS = new ArrayList<PandaRobot>();

	public static Map<String, PandaRobot> ITEM_MAP = new HashMap<String, PandaRobot>();
	public static Map<String, String> AVAILABILITY_MAP = new HashMap<String, String>();

	static {
		// Add 6 robots. - TODO?: add dynamically (what input?)
		addItem(new PandaRobot("1", "192.168.201"));
		addItem(new PandaRobot("2", "192.168.202"));
		addItem(new PandaRobot("3", "192.168.203"));
		addItem(new PandaRobot("4", "192.168.204"));
		addItem(new PandaRobot("5", "192.168.205"));
		addItem(new PandaRobot("6", "192.168.206"));
	}

	private static void addItem(PandaRobot item) {
		ITEMS.add(item);
		ITEM_MAP.put(item.id, item);
	}

	private static void addAvailability(PandaRobot item) {
		AVAILABILITY_MAP.put(item.id, item.isRobotAvailable());
	}
	
	/**
	 * A PandaRobot item.
	 */
	public static class PandaRobot {
		public String id;
		public String ROBOT_IP;
		public Integer ROBOT_PORT;
		private String state;
		
		public PandaRobot(String id, String ip) {
			this.id = id;
			this.ROBOT_IP = ip;
		}
		
		public PandaRobot(String id, String ip, Integer port) {
			this.id = id;
			this.ROBOT_IP = ip;
			this.ROBOT_PORT = port;
			state = "Robot is inactive";
		}

		@Override
		public String toString() {
			return "Robot " + id;
		}
		
		// TODO: implement On Button Pressed - change Button text to "[Robot_Id] Active" or "[Robot_Id] Inactive"
		public String isRobotAvailable(){
			Log.i("SEND", "\n\n   CHECKING IF THE ROBOT  " + id + "  IS AVAILABLE\n\n");
			return "Robot is active.";
			/*try {
				Socket socket = new Socket(ROBOT_IP, ROBOT_PORT);
				new DataOutputStream(socket.getOutputStream()).writeUTF("ROBOT_TEST_IS_ALIVE");
				return true;
			} catch (IOException ioE) {
				Log.i("SEND", "\n\n   SOCKET FAIL !!!   \n\n" + ioE.getMessage());
				ioE.printStackTrace();
				return false;
			}*/
		}

		
		public void deactivate() {
			Log.i("SEND", "\n\n   DEACTIVATING ROBOT  " + id + "  \n\n");
			
			/* TODO:   REMOVE THIS ROBOT FROM THE GLOBAL CONTROL LIST
			 * USED BY CONTROLLERACTIVITY ! ??
			 */
		}
	}
	
}
