 package com.example.voicecontroller;


import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.Button;

/**
 * An activity representing a list of Robots. This activity has different
 * presentations for handset and tablet-size devices. On handsets, the activity
 * presents a list of items, which when touched, lead to a
 * {@link RobotDetailActivity} representing item details. On tablets, the
 * activity presents the list of items and item details side-by-side using two
 * vertical panes.
 * <p>
 * The activity makes heavy use of fragments. The list of items is a
 * {@link RobotListFragment} and the item details (if present) is a
 * {@link RobotDetailFragment}.
 * <p>
 * This activity also implements the required
 * {@link RobotListFragment.Callbacks} interface to listen for item selections.
 */
public class RobotListActivity extends FragmentActivity implements
		RobotListFragment.Callbacks {

	/**
	 * Whether or not the activity is in two-pane mode, i.e. running on a tablet
	 * device.
	 */
	
	private boolean mTwoPane;
	private Button startControllButton;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);	
		
		setContentView(R.layout.activity_robot_list);

		if (findViewById(R.id.robot_detail_container) != null) {
			// The detail container view will be present only in the
			// large-screen layouts (res/values-large and
			// res/values-sw600dp). If this view is present, then the
			// activity should be in two-pane mode.
			mTwoPane = true;

			// In two-pane mode, list items should be given the
			// 'activated' state when touched.
			((RobotListFragment) getSupportFragmentManager().findFragmentById(
					R.id.robot_list)).setActivateOnItemClick(true);
		}
		
		startControllButton = (Button) findViewById(R.id.startcontrol);
		startControllButton.getBackground().setColorFilter(0xFFFF0000, PorterDuff.Mode.MULTIPLY);

		// TODO: If exposing deep links into your app, handle intents here.
	}
	
	
	
	/**
	 * Callback method from {@link RobotListFragment.Callbacks} indicating that
	 * the item with the given ID was selected.
	 */
	@Override
	public void onItemSelected(String id) {
		if (mTwoPane) {
			// In two-pane mode, show the detail view in this activity by
			// adding or replacing the detail fragment using a
			// fragment transaction.
			Bundle arguments = new Bundle();
			arguments.putString(RobotDetailFragment.ARG_ITEM_ID, id);
			RobotDetailFragment fragment = new RobotDetailFragment();
			fragment.setArguments(arguments);
			getSupportFragmentManager().beginTransaction()
					.replace(R.id.robot_detail_container, fragment).commit();

		} else {
			// In single-pane mode, simply start the detail activity
			// for the selected item ID.
			Intent detailIntent = new Intent(this, RobotDetailActivity.class);
			detailIntent.putExtra(RobotDetailFragment.ARG_ITEM_ID, id);
			startActivity(detailIntent);
		}
	}
	
	public void startControl(View view){
		Intent detailIntent = new Intent(this, ControllerActivity.class);
		startActivity(detailIntent);
	}
}
