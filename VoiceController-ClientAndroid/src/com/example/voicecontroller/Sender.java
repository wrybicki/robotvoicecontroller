package com.example.voicecontroller;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

import android.util.Log;


public class Sender implements Runnable{
	private Set<Integer> robots;
	
	public Sender(){
		robots = new HashSet<Integer>();
	}
	
	public void registerRobot(int robotID) {
		robots.add(robotID);
	}
	
	public void unregisterRobot(int robotID) {
		for(Integer robot: robots)
			if(robot.equals(robotID))
				robots.remove(robot);
	}
	
	
	public void sendCommand(String host, int port, Integer robotID, String command) {
		
		Socket socket = createSocket(host, port);
		
		try {
			DataOutputStream out = new DataOutputStream(socket.getOutputStream());
			
			String full_command = robotID.toString() + " " + command;
			out.writeUTF(full_command);
			System.out.println("Sent command '" + full_command + "' to robot "
					+ robotID);
			
			out.close();
			
		} catch (IOException ioE) {
			ioE.printStackTrace();
		} finally {
			try {
				socket.close();
			} catch (IOException ioE ) {
				Log.i("SEND", ioE.getMessage());
				ioE.printStackTrace();
			}
		}
	}
	
	@Override
	public void run() {
		Scanner scanner = new Scanner(System.in);
		while(true) {
			String command = scanner.nextLine();
		}
	}

	private Socket createSocket(String host, int port) {
		try {
			Socket socket = new Socket(host, port);
			return socket;
		} catch (IOException ioE) {
			Log.i("SEND", "\n\n SOCKET FAIL !!! \n\n" + ioE.getMessage());
			ioE.printStackTrace();
		}
		return null;
	}
}



