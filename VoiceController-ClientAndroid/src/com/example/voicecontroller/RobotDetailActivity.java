package com.example.voicecontroller;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.NavUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.example.listcontent.ListContent;

/**
 * An activity representing a single Robot detail screen. This activity is only
 * used on handset devices. On tablet-size devices, item details are presented
 * side-by-side with a list of items in a {@link RobotListActivity}.
 * <p>
 * This activity is mostly just a 'shell' activity containing nothing more than
 * a {@link RobotDetailFragment}.
 */
public class RobotDetailActivity extends FragmentActivity 
{
	private int PORT = 6666;
	private String IP = "192.168.1.101";
	private static final String PREFS_NAME = "pl.edu.agh.voicecontroller";
	
	private ListContent.PandaRobot mItem;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_robot_detail);
	
		
		AlertDialog.Builder addressAlert = new AlertDialog.Builder(this);
		addressAlert.setTitle("Connect to robot...");
		addressAlert.setMessage("Select IP and port ([IP]:[Port]):");
		final EditText input = new EditText(this);
		final Context activityContext = (Context) this;
		
		input.setText(loadIPAddressPref(this));
		addressAlert.setView(input);
		addressAlert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {
				String addr = input.getText().toString();
				saveIPAddressPref(activityContext, addr);
				
				
				 /*TODO:
				 * 1. Connect to specified robot and display message about the connection state
				 * (robotAvailable / robot unreachable)
				 * 2. Set the button color in the previous activity to Green / Red?*/
				 
			}
		});
		addressAlert.show();
		
		
		// Show the Up button in the action bar.
		getActionBar().setDisplayHomeAsUpEnabled(true);

		// savedInstanceState is non-null when there is fragment state
		// saved from previous configurations of this activity
		// (e.g. when rotating the screen from portrait to landscape).
		// In this case, the fragment will automatically be re-added
		// to its container so we don't need to manually add it.
		// For more information, see the Fragments API guide at:
		//
		// http://developer.android.com/guide/components/fragments.html
		//
		if (savedInstanceState == null) {
			// Create the detail fragment and add it to the activity
			// using a fragment transaction.
			Bundle arguments = new Bundle();
			arguments.putString(RobotDetailFragment.ARG_ITEM_ID, getIntent()
					.getStringExtra(RobotDetailFragment.ARG_ITEM_ID));
			RobotDetailFragment fragment = new RobotDetailFragment();
			fragment.setArguments(arguments);
			getSupportFragmentManager().beginTransaction()
					.add(R.id.robot_detail_container, fragment).commit();
		}
	}
	
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			// This ID represents the Home or Up button. In the case of this
			// activity, the Up button is shown. Use NavUtils to allow users
			// to navigate up one level in the application structure. For
			// more details, see the Navigation pattern on Android Design:
			//
			// http://developer.android.com/design/patterns/navigation.html#up-vs-back
			//
			NavUtils.navigateUpTo(this, new Intent(this,
					RobotListActivity.class));
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	
	public void deactivate(View view){
		Bundle arguments = new Bundle();
		arguments.putString(RobotDetailFragment.ARG_ITEM_ID, getIntent()
				.getStringExtra(RobotDetailFragment.ARG_ITEM_ID));
		
		if (arguments.containsKey(RobotDetailFragment.ARG_ITEM_ID)) {
			// Load the dummy content specified by the fragment
			// arguments. In a real-world scenario, use a Loader
			// to load content from a content provider.
			mItem = ListContent.ITEM_MAP.get(arguments.getString(
					RobotDetailFragment.ARG_ITEM_ID));
			
			mItem.deactivate();
			
			((TextView) view.findViewById(R.id.robot_detail))
					.setText(mItem.isRobotAvailable());
		}
	}
	
	
	static String loadIPAddressPref(Context context) {
		SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME, 0);
		String prefix = prefs.getString("ipaddr", null);
		if (prefix != null) {
			return prefix;
		} else {
			return "";
		}
	}

	static void saveIPAddressPref(Context context, String text) {
		SharedPreferences.Editor prefs = context
		    .getSharedPreferences(PREFS_NAME, 0).edit();
		prefs.putString("ipaddr", text);
		prefs.commit();
	}
}
