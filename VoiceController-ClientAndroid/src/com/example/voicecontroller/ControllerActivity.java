package com.example.voicecontroller;

import android.view.Menu;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.os.StrictMode;
import android.speech.RecognizerIntent;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


public class ControllerActivity extends Activity 
{
	private static final int VOICE_RECOGNITION_REQUEST_CODE = 1001;

	
	// TODO - add Another activity to select HOST, PORT and ROBOT(s?) from the phone
	private static final String HOST = "127.0.0.1"; // 
	private static final Integer PORT = 6666;
	private static final Integer ROBOT_ID = 3;
	
	//"10.20.111.184";//192.168.1.101";  
	//"192.168.2.100";//"10.20.111.86"; //"192.168.1.101"; //"192.168.1.102";	
	
	
	private EditText recognizedText;
	private Button speakButton;
	private Button forwardButton;
	private Button backButton;
	private Button leftButton;
	private Button rightButton;
	private Button stopButton;
	
	
	private Sender sender;

	@Override
	public void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_controller);
		
		
		sender = new Sender();
		
		// Solution for Android 9 and higher. For Android 8 and lower must be commented out.
		
		// TODO - implement the better solution.
		StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().
				permitAll().build();
		StrictMode.setThreadPolicy(policy);
		
		recognizedText = (EditText) findViewById(R.id.etTextHint);
		
		speakButton = (Button) findViewById(R.id.speak);
		speakButton.getBackground().setColorFilter(0xFF00F000, PorterDuff.Mode.MULTIPLY);
		forwardButton = (Button) findViewById(R.id.forward);
		leftButton = (Button) findViewById(R.id.left);
		rightButton = (Button) findViewById(R.id.right);
		backButton = (Button) findViewById(R.id.back);
		stopButton = (Button) findViewById(R.id.stop);
		stopButton.getBackground().setColorFilter(0xFFFF0000, PorterDuff.Mode.MULTIPLY);
		checkVoiceRecognition();
	}

	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.controller, menu);
		return true;
	}
	
	
	public void checkVoiceRecognition() 
	{
		// Check if voice recognition is present
		PackageManager pm = getPackageManager();
		List<ResolveInfo> activities = pm.queryIntentActivities(new Intent(
				RecognizerIntent.ACTION_RECOGNIZE_SPEECH), 0);
		
		if (activities.size() == 0) 
		{
			speakButton.setEnabled(false);
			Toast.makeText(this, "Voice recognizer not present",
					Toast.LENGTH_SHORT).show();
		}
	}

	
	public void speak(View view) 
	{
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        
        // Specify the calling package to identify your application
        intent.putExtra(RecognizerIntent.EXTRA_CALLING_PACKAGE, getClass()
                        .getPackage().getName());

        // Display an hint to the user about what he should say.
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT, recognizedText.getText()
                        .toString());

        // Given an hint to the recognizer about what the user is going to say
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                        RecognizerIntent.LANGUAGE_MODEL_WEB_SEARCH);

        startActivityForResult(intent, VOICE_RECOGNITION_REQUEST_CODE);
    }
	
	
	
	public void forward(View view){
		send("forward", view);
	}
	
	public void back(View view){
		send("back", view);
	}
	
	public void right(View view){
		send("right", view);
	}
	
	public void left(View view){
		send("left", view);
	}
	
	public void stop(View view){
		send("stop", view);
	}
	
	private void send(String command, View view)
	{
		Log.i("SEND", "Wyslano komende " + command);
		sender.sendCommand(HOST, PORT, ROBOT_ID, command);
	}
	
	public void mainMenu(View view){
		Intent detailIntent = new Intent(this, RobotListActivity.class);
		startActivity(detailIntent);
	}


	//@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) 
	{
		if (requestCode == VOICE_RECOGNITION_REQUEST_CODE)
		{
			if(resultCode == RESULT_OK) 
			{
				List <String> possibleDirections = 
						Arrays.asList("przód", "tył", "prawo", "lewo");
				List <String> possibleSpeeds = 
						Arrays.asList("1", "2", "3", "4", "5", "6", "7", "8", "9", "10");
				
				ArrayList<String> textMatchList = data
						.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);

				if (!textMatchList.isEmpty()) 
				{					
					ArrayAdapter<String> list = new ArrayAdapter<String>(this,
							android.R.layout.simple_list_item_1,
							textMatchList);
					
					String command = list.getItem(0);
					
					if("".equals(command))
						// @TODO change to read from xml file.
						recognizedText.setText("Powiedz: 'przód', 'tył', 'prawo', 'lewo'" +
								"lub liczbę z zakresu 1-10, oznaczającą szybkość robota");	
					
					else if(possibleDirections.contains(command))
					{
						if(command.equals("przód"))
							sender.sendCommand(HOST, PORT, ROBOT_ID, "forward");
						else if(command.equals("tył"))
							sender.sendCommand(HOST, PORT, ROBOT_ID, "back");
						else if(command.equals("prawo"))
							sender.sendCommand(HOST, PORT, ROBOT_ID, "right");
						else if(command.equals("lewo"))
							sender.sendCommand(HOST, PORT, ROBOT_ID, "left");
						
						recognizedText.setText("Kierunek: " + command);
							
					} 
					else if(possibleSpeeds.contains(command)) 
					{
						sender.sendCommand(HOST, PORT, ROBOT_ID, "command");
						recognizedText.setText("Ustawiłeś szybkość na " + command + "/10");
					} else
						recognizedText.setText("'" + command + "' nierozpoznane");
											
				}
			//Result code for various errors.	
			}else if(resultCode == RecognizerIntent.RESULT_AUDIO_ERROR){
				showToastMessage("Audio Error");
			}else if(resultCode == RecognizerIntent.RESULT_CLIENT_ERROR){
				showToastMessage("Client Error");
			}else if(resultCode == RecognizerIntent.RESULT_NETWORK_ERROR){
				showToastMessage("Network Error");
			}else if(resultCode == RecognizerIntent.RESULT_NO_MATCH){
				showToastMessage("No Match");
			}else if(resultCode == RecognizerIntent.RESULT_SERVER_ERROR){
				showToastMessage("Server Error");
			}
		}
		super.onActivityResult(requestCode, resultCode, data);
	}
	
	void showToastMessage(String message){
		Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
	}
}