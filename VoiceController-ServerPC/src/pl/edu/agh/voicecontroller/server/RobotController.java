package pl.edu.agh.voicecontroller.server;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import pl.edu.agh.amber.roboclaw.RoboclawProxy;

public class RobotController {
	
	private int speed = 500;
	private int move_time = 500;
	
	
	public void executeCommand(String _command, RoboclawProxy proxy) 
			throws NumberFormatException, IOException, InterruptedException {
				
		List <String> possibleSpeeds = 
				Arrays.asList("1", "2", "3", "4", "5", "6", "7", "8", "9", "10");
		
		System.out.println("RobotController executing command: " + _command.toString());
		
		/* circle int_size_1-10 */
		if("circle".equals(_command))
			drawCircle(proxy, move_time);
		
		/* forward int_timeDeciSeconds int_speed_1-10 */
		else if("forward".equals(_command))
			goForward(proxy, move_time, speed);
		
		/* back int_timeDeciSeconds int_speed_1-10 */
		else if("back".equals(_command))
			goBack(proxy, move_time, speed);
		
		/* turn_right */
		else if("turn_right".equals(_command))
			turnRight(proxy);

		/* turn_right */
		else if("turn_left".equals(_command))
			turnLeft(proxy);
		
		else if(possibleSpeeds.contains(_command))
			setSpeed(Integer.parseInt(_command));
		
		else
			System.err.println("Unrecognized command: " + _command);
			
	}
	
	private void setSpeed(int newSpeed) {		
		speed = newSpeed * 400;
	}

	private void drawCircle(RoboclawProxy proxy, int sizeDeciMeters) 
			throws IOException, InterruptedException{
		int frontLeftSpeed = 100, frontRightSpeed = 560, rearLeftSpeed = 100, rearRightSpeed = 560;
		proxy.sendMotorsCommand(frontLeftSpeed, frontRightSpeed, rearLeftSpeed, rearRightSpeed);
		Thread.sleep(8 * 1000);
		proxy.sendMotorsCommand(0, 0, 0, 0);
	}

	private void goForward(RoboclawProxy proxy, int speed, int timeDeciSeconds) 
			throws IOException, InterruptedException {
		int sp = 100 * speed;
		int frontLeftSpeed = sp, frontRightSpeed = sp, rearLeftSpeed = sp, rearRightSpeed = sp;
		proxy.sendMotorsCommand(frontLeftSpeed, frontRightSpeed, rearLeftSpeed, rearRightSpeed);
		Thread.sleep(100 * timeDeciSeconds);
		proxy.sendMotorsCommand(0, 0, 0, 0);
	}
	
	private void goBack(RoboclawProxy proxy, int speed, int timeDeciSeconds) 
			throws IOException, InterruptedException {
		int sp = -100 * speed;
		int frontLeftSpeed = sp, frontRightSpeed = sp, rearLeftSpeed = sp, rearRightSpeed = sp;
		proxy.sendMotorsCommand(frontLeftSpeed, frontRightSpeed, rearLeftSpeed, rearRightSpeed);
		Thread.sleep(100 * timeDeciSeconds);
		proxy.sendMotorsCommand(0, 0, 0, 0);
	}
	
	private void turnRight(RoboclawProxy proxy) throws IOException, InterruptedException {
		int frontLeftSpeed = 100, frontRightSpeed = 0, rearLeftSpeed = 100, rearRightSpeed = 0;
		proxy.sendMotorsCommand(frontLeftSpeed, frontRightSpeed, rearLeftSpeed, rearRightSpeed);
		Thread.sleep(1000);
		proxy.sendMotorsCommand(0, 0, 0, 0);
	}
	
	private void turnLeft(RoboclawProxy proxy) throws IOException, InterruptedException {
		int frontLeftSpeed = 0, frontRightSpeed = 110, rearLeftSpeed = 0, rearRightSpeed = 110;
		proxy.sendMotorsCommand(frontLeftSpeed, frontRightSpeed, rearLeftSpeed, rearRightSpeed);
		Thread.sleep(1000);
		proxy.sendMotorsCommand(0, 0, 0, 0);
	}
	
}
