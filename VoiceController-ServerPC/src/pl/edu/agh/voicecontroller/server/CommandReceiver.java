package pl.edu.agh.voicecontroller.server;
import java.io.DataInputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;

import pl.edu.agh.amber.common.AmberClient;
import pl.edu.agh.amber.roboclaw.RoboclawProxy;


public class CommandReceiver implements Runnable 
{
	private final int SERVER_PORT;
	private Integer ROBOTS_PORT;
	private String ROBOTS_HOST_START;
	
	private final RobotController robotController;
	
	private Map<Integer, AmberClient> clients = new HashMap<Integer, AmberClient>();
	private Map<Integer, RoboclawProxy> proxies = new HashMap<Integer, RoboclawProxy>();
	
	
	public CommandReceiver(int serverPort, int robotsPort, String robotsHost) 
	{
		SERVER_PORT = serverPort;
		ROBOTS_PORT = robotsPort; 
		ROBOTS_HOST_START = robotsHost;
		robotController = new RobotController();
	}
	
	
	public String receiveCommand() 
	{		
		ServerSocket listenSocket = createListenSocket(SERVER_PORT);
		Socket clientSocket;
		String command = null;
		
		try {		
	
			/* Accepts new connection */
			System.out.println("Waiting for new client-connection...\n");
			clientSocket = listenSocket.accept();
			System.out.println("New client-socket created");
			
			/* Reads file size */
			DataInputStream in = new DataInputStream(clientSocket.getInputStream());
			command = in.readUTF();
			
			in.close();
			clientSocket.close();
			
		} catch (IOException ioE) {
			ioE.printStackTrace();
		} finally {
			try {
				listenSocket.close();
			} catch (IOException ioE) {
				ioE.printStackTrace();
			}
		}
		return command;
	}
	
	
	private ServerSocket createListenSocket(int port) 
	{
		try {
			ServerSocket listenSocket = new ServerSocket(port);	
			return listenSocket;
		} catch (IOException ioE) {
			ioE.printStackTrace();
		}
		return null;
	}
	
	
	private void terminateClients() 
	{
		for (AmberClient client: clients.values()) {
			client.terminate();
		}
	}

	
	private void registerProxy(int robotId) throws IOException  
	{
		int lastOctet = 200 + robotId;
		
		AmberClient client;
	
		client = new AmberClient(ROBOTS_HOST_START + lastOctet, ROBOTS_PORT);
		clients.put(robotId, client);
		
		RoboclawProxy roboclawProxy = new RoboclawProxy(client, 0);
		proxies.put(robotId, roboclawProxy);
	}
		
	
	@Override
	public void run() 
	{
		while(true) 
		{
			/* Thread blocks waiting for command from the client */
			String command = receiveCommand();
		
			System.out.println("Received command [robotID command]: '" + command +"'");
						
			Integer robotId = Integer.parseInt(command.substring(0,1));
			//try {
				System.out.println(">>> registerProxy(robotId)");
				//registerProxy(robotId);
				System.out.println(">>> robotController.executeCommand(command.substring(2), proxies.get(robotId))\n");
				//robotController.executeCommand(command.substring(2), proxies.get(robotId));
				
//			} catch (NumberFormatException e){
//				e.printStackTrace();
//			}  catch (IOException e) {
//				e.printStackTrace();
//			} catch (InterruptedException e) {
//				e.printStackTrace();
//			}
			
		}
	}
}