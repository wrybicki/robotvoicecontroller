package pl.edu.agh.voicecontroller.server;


public class Server 
{
	private static final Integer SERVER_PORT = 6666;	
	private static final Integer ROBOTS_PORT = 26233;
	private static final String ROBOTS_HOST_START = "192.168.2.";
	
	
	public static void main(String[] args) {
		new CommandReceiver(SERVER_PORT, ROBOTS_PORT, ROBOTS_HOST_START).run();
	}
}
