package pl.edu.agh.voicecontroller.server;
import java.io.DataInputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

/* For later use - to read 9dofData from Sensors */


public class SensorDataSender implements Runnable 
{
	public String receiveCommand(int port) 
	{		
		ServerSocket listenSocket = createListenSocket(port);
		Socket clientSocket;
		String command = null;
		
		try {		
			while (true) {
				/* Accepts new connection */
				clientSocket = listenSocket.accept();
				
				DataInputStream in = new DataInputStream(clientSocket.getInputStream());
				command = in.readUTF();
				
				in.close();
				clientSocket.close();
			}
			
		} catch (IOException ioE) {
			ioE.printStackTrace();
		} finally {
			try {
				listenSocket.close();
			} catch (IOException ioE) {
				ioE.printStackTrace();
			}
		}
		return command;
	}
	
	
	private ServerSocket createListenSocket(int port) {
		try {
			ServerSocket listenSocket = new ServerSocket(port);	
			return listenSocket;
		} catch (IOException ioE) {
			ioE.printStackTrace();
		}
		return null;
	}


	@Override
	public void run() {
		while(true) {
			/* Do something - implement later to get data from 9dof sensors */
		}
	}
}	